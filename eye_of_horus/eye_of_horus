################################################################################
## File: eye_of_horus.py
## Description:
##    Eye of Horus takes a text file containing an appropriately formated list
##    of course sections at Rutgers University. For each section in the input
##    file, EOH checks to see if the course is open by querying the Rutgers
##    Schedule of Online Classes. If the course is open, then EOH will register
##    you for the class and remove the section from the text file before
##    sending notifications to your phone and email. Intended to be used as a
##    cronjob. Proof of concept only.
##
################################################################################
#!/usr/bin/python2


# ------------------------------------------------------------------------------
import sys
import json
from smtplib import SMTP
from urllib import urlopen, urlencode
from mechanize import Browser
from cookielib import LWPCookieJar

# SOC and Webreg
SEMESTER = '92014'
CAMPUS = 'NB'
LEVEL = 'U'
QUERY = 'http://sis.rutgers.edu/soc/courses.json?'
NETID = ''
PASSWD = ''

# Email/sms
SMTP_SERVER = 'stmp.gmail.com:587'
SMTP_USER = ''
SMTP_PASS = ''
FROM_ADDR = ''
TO_ADDR   = ''
TO_CELL   = ''
REG_MESSAGE = 'Eye of Horus strikes again... check Webreg to see what you\'re\
        signed up for.'
COMPLETE_MESSAGE = "You've managed to register for all of your courses.\n\
        Do a victory dance, right now."

# clockwork stuff
CAS = 'https://sims.rutgers.edu/webreg/chooseSemester.htm?login=cas'
EDIT_SCHED_A = 'https://sims.rutgers.edu/webreg/editSchedule.htm?'+\
        'semesterSelection='
EDIT_SCHED_B = '&submit=Continue'
ADD_COURSE = 'https://sims.rutgers.edu/webreg/addCourses.htm'
LOGOUT = 'https://sims.rutgers.edu/webreg/logout.htm'
UA_STR = 'Mozilla/5.0 (compatible; bingbot/2.0; '+\
		'+http://www.bing.com/bingbot.htm)'

# misc controls
DEBUG_MODE = False

def\ #--------------------------------------------------------------------------
eye_of_horus():

    if len(sys.argv) != 2:
        'Need an input file.'
        return

    input_file = sys.argv[1]

    outputList = []
    with open(input_file, 'r') as inHandle:

        for line in inHandle:

            course = line.rstrip('\n').split(':')
            if len(course) != 3:
                print "Please specify a real course number."
                return
            subject = course[0]
            course_num = course[1]
            sect_num = course[2]

            sectionOpen,index =  querySOC(subject,\
                                    SEMESTER,\
                                    CAMPUS,\
                                    LEVEL,\
                                    course_num,\
                                    sect_num)

            if sectionOpen:
                clockwork(index=index,\
                        semester=SEMESTER,\
                        user=NETID,\
                        passwd=PASSWD)
                sendMessage(REG_MESSAGE)
            else:
                outputList.append(line)

    if len(outputList) == 0:
        sendMessage(COMPLETE_MESSAGE)
        return

    with open(input_file, 'w') as outHandle:
        for line in outputList:
            outHandle.write(line)

def\ #--------------------------------------------------------------------------
querySOC(subject, semester, campus, level, courseNum, sectNum):

    query_str = QUERY+'subject='+subject+'&semester='+semester+\
            '&campus='+campus+'&level='+level
    response = urlopen(query_str)
    data = json.load(response)

    for i in range(0, len(data)):

        if data[i]['courseNumber'] == courseNum:
            for j in range(0, len(data[i]['sections'])):
                if data[i]['sections'][j]['number'] == sectNum:
                    if data[i]['sections'][j]['openStatus']:
                        index = data[i]['sections'][j]['index']
                        return True,index
                    else:
                        return False,''
    return False,''

def\ #--------------------------------------------------------------------------
sendMessage(message):

    server = SMTP(SMTP_SERVER)
    server.ehlo()
    server.starttls()
    server.login(SMTP_USER, SMTP_PASS)
    server.sendmail(FROM_ADDR, [TO_ADDR, TO_CELL], message)
    server.close()

def\ #--------------------------------------------------------------------------
clockwork(index, semester, user, passwd):

    # initialize the 'browser' ;)
    browser = Browser()

    cj = LWPCookieJar()
    browser.set_cookiejar(cj)

    browser.addheaders = [('User-agent', UA_STR)]
    browser.set_handle_equiv(True)
    browser.set_handle_gzip(True)
    browser.set_handle_redirect(True)
    browser.set_handle_referer(True)
    browser.set_handle_robots(False)

    # open auth page
    browser.open(CAS)

    # find the login form so we can tiptoe through CAS
    form_count = 0
    for f in browser.forms():
        if str(f.attrs['id']) == 'fm1':
            break
        form_count += 1

    # log into webreg through CAS
    browser.select_form(nr=form_count)
    browser['username'] = user
    browser['password'] = passwd
    response = browser.submit()

    # DEBUG - make sure we're actually logged in
    if DEBUG_MODE:
        print response.read()

    # navigate to 'edit schedule' page
    response = browser.open(EDIT_SCHED_A+str(semester)+EDIT_SCHED_B)

    # DEBUG - make sure that semester was selected correctly
    if DEBUG_MODE:
        print response.read()

    # CRAFT A POST REQUEST
    parameters = {
            'coursesToAdd[0].courseIndex' : index,
            'coursesToAdd[1].courseIndex' : '',
            'coursesToAdd[2].courseIndex' : '',
            'coursesToAdd[3].courseIndex' : '',
            'coursesToAdd[4].courseIndex' : '',
            'coursesToAdd[5].courseIndex' : '',
            'coursesToAdd[6].courseIndex' : '',
            'coursesToAdd[7].courseIndex' : '',
            'coursesToAdd[8].courseIndex' : '',
            'coursesToAdd[9].courseIndex' : ''
    }
    data = urlencode(parameters)
    response = browser.open(ADD_COURSE,data)

    # DEBUG - did it work?
    if DEBUG_MODE:
        print response.read()

    # log out
    browser.open(LOGOUT)

if __name__ == '__main__':
    eye_of_horus()
